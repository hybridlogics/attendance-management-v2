import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()

export class PunchService {

    constructor(private _http: Http) {}

    punchIn(user) {
        var data = {'data': {'userId':user.userId}};

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._http.post('http://192.168.10.45:4567/api/Attendences/punchIn?access_token=' + user.id, data,{headers:headers})
        .map(res => res.json());
    }

    punchOut(user, punchIn) {
        var data = {'data': {'userId':user.userId, 'punchIn': punchIn}};

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._http.post('http://192.168.10.45:4567/api/Attendences/punchOut?access_token=' + user.id, data,{headers:headers})
        .map(res => res.json());
    }

    getEmployeeAttendence(user) {
        return this._http.get('http://192.168.10.45:4567/api/Attendences?access_token=' + user.id)
        .map(res => res.json());
    }

    getAllUsers(user) {
        return this._http.get('http://192.168.10.45:4567/api/AppUsers?access_token=' + user.id)
        .map(res => res.json());
    }

    deleteUserbyId(user, employeeId) {
        return this._http.delete('http://192.168.10.45:4567/api/AppUsers/' + employeeId + '?access_token=' + user.id)
        .map(res => res.json());
    }

    updateUserbyId(data, user, employeeId) {

        let headers = new Headers();
        headers.append('Content-Type', 'application/json');

        return this._http.patch('http://192.168.10.45:4567/api/AppUsers/' + employeeId + '?access_token=' + user.id, data,{headers:headers})
        .map(res => res.json());
    }

    changePunchTimings(user, attendenceId, data) {
        return this._http.patch('http://192.168.10.45:4567/api/Attendences/' + attendenceId + '?access_token=' + user.id, data)
        .map(res => res.json());
    }
}