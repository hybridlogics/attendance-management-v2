"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var PunchService = (function () {
    function PunchService(_http) {
        this._http = _http;
    }
    PunchService.prototype.punchIn = function (user) {
        var data = { 'data': { 'userId': user.userId } };
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post('http://192.168.10.45:4567/api/Attendences/punchIn?access_token=' + user.id, data, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    PunchService.prototype.punchOut = function (user, punchIn) {
        var data = { 'data': { 'userId': user.userId, 'punchIn': punchIn } };
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.post('http://192.168.10.45:4567/api/Attendences/punchOut?access_token=' + user.id, data, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    PunchService.prototype.getEmployeeAttendence = function (user) {
        return this._http.get('http://192.168.10.45:4567/api/Attendences?access_token=' + user.id)
            .map(function (res) { return res.json(); });
    };
    PunchService.prototype.getAllUsers = function (user) {
        return this._http.get('http://192.168.10.45:4567/api/AppUsers?access_token=' + user.id)
            .map(function (res) { return res.json(); });
    };
    PunchService.prototype.deleteUserbyId = function (user, employeeId) {
        return this._http.delete('http://192.168.10.45:4567/api/AppUsers/' + employeeId + '?access_token=' + user.id)
            .map(function (res) { return res.json(); });
    };
    PunchService.prototype.updateUserbyId = function (data, user, employeeId) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this._http.patch('http://192.168.10.45:4567/api/AppUsers/' + employeeId + '?access_token=' + user.id, data, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    PunchService.prototype.changePunchTimings = function (user, attendenceId, data) {
        return this._http.patch('http://192.168.10.45:4567/api/Attendences/' + attendenceId + '?access_token=' + user.id, data)
            .map(function (res) { return res.json(); });
    };
    PunchService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], PunchService);
    return PunchService;
}());
exports.PunchService = PunchService;
//# sourceMappingURL=punch.service.js.map