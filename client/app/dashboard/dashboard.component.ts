import { Component } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import {Router} from '@angular/router';

@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent{

    storage: LocalStorageService;
  navigator: Router;

  constructor(_router: Router, local: LocalStorageService) {
      
    this.storage=local;
    this.navigator=_router;
    if(!this.storage.get('id')) { this.navigator.navigateByUrl('/login'); }
  }

}
