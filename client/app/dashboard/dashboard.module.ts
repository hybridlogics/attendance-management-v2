import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common'; 
import { RouterModule } from '@angular/router';
import { MODULE_COMPONENTS, MODULE_ROUTES } from './dashboard.routes';
import { ChartsModule } from 'ng2-charts';
import { PunchService } from './punch.service';
import { FormsModule } from '@angular/forms';


@NgModule({
    imports: [
        CommonModule,
        ChartsModule,
        FormsModule,
        RouterModule.forChild(MODULE_ROUTES)
    ],
    declarations: [ MODULE_COMPONENTS ],
    providers: [PunchService]
})

export class DashboardModule{}
