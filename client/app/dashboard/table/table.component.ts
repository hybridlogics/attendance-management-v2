import { Component, OnInit, trigger, state, style, transition, animate } from '@angular/core';
import { Router } from "@angular/router";
import { PunchService } from "../punch.service";
import { Http } from "@angular/http";






declare var $: any;

@Component({
    selector: 'table-cmp',
    moduleId: module.id,
    templateUrl: 'table.component.html',
    styleUrls: ['table.component.css']
})

export class TableComponent implements OnInit {
    diff: any;
    loggedUser: any;




    currentDate: Date = new Date();
    currentMonth: any;
    currentYear: any;

    analytics: boolean = false;
    flag: boolean = false;
    currentUser: any;

    users = []
    monthDays: any;

    monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    daysName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    years = [
        { name: '2017', id: 2017 },
        { name: '2018', id: 2018 },
        { name: '2019', id: 2019 },
        { name: '2020', id: 2020 },
        { name: '2021', id: 2021 },
        { name: '2022', id: 2021 }];

    userMonthlyData = [];
    weekHourSum = [0, 0, 0, 0, 0, 0, 0];
    attendenceList = [];
    allPunchs = [];
    OWD: any = 0;
    WD: any = 0;
    WH: any = 0;
    HW: any = 0;
    L: any = 0;
    A: any = 0;

    constructor(private navigator: Router, private punch: PunchService, private http: Http) {
        if (localStorage.getItem('userRole') !== 'admin') {
            this.navigator.navigateByUrl('/dashboard');
        }
    }

    ngOnInit() {
        this.loggedUser = JSON.parse(localStorage.getItem('my-app.id'))

        this.getUsers();

        this.currentYear = new Date().getFullYear();
        this.currentMonth = this.monthNames[this.currentDate.getMonth()];

        this.hideAnalytics();
    }

    ngAfterViewInit() {

    }

    fabVisible() {
        if ($(".backdrop").is(":visible")) {
            $(".backdrop").fadeOut(125);
            $(".fab.child")
                .stop()
                .animate({
                    bottom: $("#masterfab").css("bottom"),
                    opacity: 0
                }, 125, function () {
                    $(this).hide();
                });
        } else {
            $(".backdrop").fadeIn(125);
            $(".fab.child").each(function () {
                $(this)
                    .stop()
                    .show()
                    .animate({
                        bottom: (parseInt($("#masterfab").css("bottom")) + parseInt($("#masterfab").outerHeight()) + 70 * $(this).data("subitem") - $(".fab.child").outerHeight()) + "px",
                        opacity: 1
                    }, 125);
            });
        }
    }

    getUsers() {
        this.punch.getAllUsers(this.loggedUser).subscribe(data => {
            this.users = data
        },
            error => console.log(error))
    }

    getAttendence() {
        var temp = 0;
        this.punch.getEmployeeAttendence(this.loggedUser).subscribe(
            data => {
                for (var index = 0; index < data.length; index++) {
                    data[index].date = new Date(Number.parseInt(data[index].date))

                    if (data[index].userId === this.currentUser.id) {
                        this.attendenceList.push(data[index]);

                        if (data[index].diff) {
                            for (var j = 0; j < this.attendenceList.length; j++) {
                                if ((data[index].date).getTime() === (this.attendenceList[j].date).getTime()) {
                                    if (temp === 1)
                                        this.attendenceList[j].diff = this.attendenceList[j].diff + data[index].diff;

                                    temp = 1;
                                }
                            }
                        }
                    }

                }
                this.getMonthDays();
            },
            error => console.log(error)
        )
    }

    index: number = 0;

    getMonthDays() {
        var year = this.currentYear;
        var month = this.monthNames.indexOf(this.currentMonth);
        this.monthDays = new Date(year, month + 1, 0).getDate()


        var myDate = new Date();
        myDate.setFullYear(year);
        myDate.setMonth(month);

        this.userMonthlyData = [];
        var monthHours = [];

        this.OWD = 0;
        this.WD = 0;
        this.WH = 0;
        this.HW = 0;
        this.L = 0;
        this.A = 0;
        this.index = 0;

        var date;

        for (var index = 1; index <= this.monthDays; index++) {


            this.monthChartLabels.push(index);

            for (this.index = 0; this.index < this.attendenceList.length; this.index++) {
                date = new Date((this.attendenceList[this.index]).date)
                if (index === date.getDate() && date.getMonth() === myDate.getMonth() && date.getFullYear() === myDate.getFullYear()) {
                    break;
                }

            }



            myDate.setDate(index);
            if (myDate.getDay() == 6) {
                this.userMonthlyData.push({ day: 'SAT', date: index });
                monthHours.push(0)
            }
            else if (myDate.getDay() == 0) {
                this.userMonthlyData.push({ day: 'SUN', date: index });
                monthHours.push(0)
            }
            else if (date && index === date.getDate() && date.getMonth() === myDate.getMonth() && date.getFullYear() === myDate.getFullYear()) {

                if ((this.attendenceList[this.index]).reason && (this.attendenceList[this.index]).punchIn === null) {
                    this.userMonthlyData.push({ day: (this.attendenceList[this.index]).reason, date: index });
                    monthHours.push(0);
                    if ((this.attendenceList[this.index]).reason === 'L')
                        this.L++;

                    if ((this.attendenceList[this.index]).reason === 'A')
                        this.A++;

                    this.OWD--;
                } else {

                    this.WD++;

                    if (myDate.getDay() == 1) {
                        this.weekHourSum = [0, 0, 0, 0, 0, 0, 0];
                    }

                    // var punchIn = new Date((this.attendenceList[this.index]).punchIn).getTime();
                    // var punchOut = new Date((this.attendenceList[this.index]).punchOut).getTime();

                    // var diff = punchOut - punchIn;
                    var diffDate = new Date((this.attendenceList[this.index]).diff);
                    var sec = diffDate.getSeconds();
                    var min = diffDate.getMinutes();
                    var hours = diffDate.getHours() - 5;
                    var totalHours = hours + min / 60 + sec / 3600;

                    this.weekHourSum[myDate.getDay()] = this.weekHourSum[myDate.getDay()] + Number.parseFloat(totalHours.toFixed(4));
                    monthHours.push(this.weekHourSum[myDate.getDay()]);

                    this.userMonthlyData.push({ day: ((hours.toString()).length === 2 ? '' : '0') + hours + ':' + ((min.toString()).length === 2 ? '' : '0') + min + ':' + ((sec.toString()).length === 2 ? '' : '0') + sec, date: index })
                }

                if (this.currentDate.getDate() >= index)
                    this.OWD++;

            } else {
                if (this.currentDate.getDate() > index && myDate.getMonth() === new Date().getMonth()) {
                    this.A++;
                    this.OWD++;
                    this.userMonthlyData.push({ day: 'A', date: index });
                } else {
                    this.userMonthlyData.push({ day: '-', date: index });
                }

                monthHours.push(0)
            }

        }

        for (var index = 0; index < monthHours.length; index++) {
            this.HW = this.HW + monthHours[index];

        }
        this.HW = Number.parseFloat(this.HW).toFixed(2);

        this.WH = Number.parseFloat(this.OWD * 9 + '').toFixed(2);

        this.monthChartData[0].data = monthHours;
        this.weekChartData[0].data = this.weekHourSum;

        if (this.OWD < 0) {
            this.OWD = 0;
            this.WH = 0;
        }
        this.flag = true;


    }


    openPopover(target: HTMLElement, data): void {
        this.allPunchs = []
        for (var index = 0; index < this.attendenceList.length; index++) {

            var date = new Date(this.attendenceList[index].date);
            var punchIn = new Date((this.attendenceList[index]).punchIn);
            var punchOut = new Date((this.attendenceList[index]).punchOut);

            if (data.date === date.getDate() && this.monthNames[date.getMonth()] === this.currentMonth) {
                var currentAttendence = this.attendenceList[index];
                var punchInHours = punchIn.getHours() + ':' + punchIn.getMinutes() + ':' + punchIn.getSeconds()
                var punchOutHours = punchOut.getHours() + ':' + punchOut.getMinutes() + ':' + punchOut.getSeconds()
                this.allPunchs.push({ 'punchIn': punchInHours, 'punchOut': punchOutHours, 'currentAttendence': { 'id': currentAttendence.id, 'date': currentAttendence.date } })
            } else {
                this.allPunchs = []
                var currentAttendence = null;
                localStorage.setItem('date', data.date)
                punchIn.setHours(0, 0, 0);
                punchOut.setHours(0, 0, 0);
            }
        }



        var myContent = `
            <table>
            <tr><td style="width:40%">Punched In: </td><td>
                    <input id="punchInHours" style="width:25%;text-align:center" value="00"></input> <span style="font-size:20px">:</span>
                    <input id="punchInMins" style="width:25%;text-align:center" value="00"></input>
                    </td><tr>
                    <tr><td style="width:40%">Punched Out: </td><td>
                    <input id="punchOutHours" style="width:25%;text-align:center" value="00"></input> <span style="font-size:20px">:</span>
                    <input id="punchOutMins" style="width:25%;text-align:center" value="00"></input>
                    </td><tr>
            </table>
            <button type="submit" class="btn btn-danger" value="" onclick="save(this.value)" style="padding:10px 10px" >Update</button>
            `


        for (var index = 0; index < this.allPunchs.length; index++) {
            myContent = 'Punched In: ' + this.allPunchs[index].punchIn + '<br>Punched Out: ' + this.allPunchs[index].punchOut + '<br><hr>' + myContent

        }

        $(target).popover({
            html: true,
            // title : '<a class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true" style="color:black"></i></a>',
            content: `<div style="max-height:200px;overflow-y:scroll"><h6><b>` + this.currentMonth + `, ` + data.date + `&nbsp;&nbsp;&nbsp;` + this.daysName[(new Date(this.currentYear + '-' + this.currentMonth + '-' + data.date)).getDay()] + `</b></h6>
                    ` + myContent + `
                    
                    <button type="submit" class="btn btn-danger" onclick="localStorage.leave = 'setLeave'" style="padding:10px 10px" >Set Leave</button>
                    </div>
                    <script>
                    function save(value) {
                        localStorage.punchInHours = $("#punchInHours").val();
                        localStorage.punchInMins = $("#punchInMins").val();

                        localStorage.punchOutHours = $("#punchOutHours").val();
                        localStorage.punchOutMins = $("#punchOutMins").val();

                        localStorage.currentAttendence = value;
                    }
                    </script>
                    `
        });


        $(document).on("click", ".btn", function () {
            // $(this).parents(".popover").popover('hide');

            if (localStorage.getItem('leave')) {
                this.setLeave(currentAttendence);
            } else if (localStorage.getItem('punchInHours')) {
                this.changeTime(localStorage.getItem('currentAttendence'));
            }

        }.bind(this));

    }

    changeTime(value) {
        if (value) {
            var res = value.split(" ");
            var currentAttendence = { id: res[0], date: new Date(res[4] + '-' + res[2] + '-' + res[3]) }

            localStorage.removeItem('currentAttendence')
        }
        // var punchIn = new Date(localStorage.getItem('punchIn'));
        // var punchOut = new Date(localStorage.getItem('punchOut'));
        if (currentAttendence) {
            var punchInHours = Number.parseInt(localStorage.getItem('punchInHours'))
            var punchInMins = Number.parseInt(localStorage.getItem('punchInMins'))
            var PunchInDate = new Date((currentAttendence.date).getFullYear(), (currentAttendence.date).getMonth(), (currentAttendence.date).getDate(), punchInHours, punchInMins)

            var punchOutHours = Number.parseInt(localStorage.getItem('punchOutHours'))
            var punchOutMins = Number.parseInt(localStorage.getItem('punchOutMins'))
            var PunchOutDate = new Date((currentAttendence.date).getFullYear(), (currentAttendence.date).getMonth(), (currentAttendence.date).getDate(), punchOutHours, punchOutMins)

            var diff = PunchOutDate.getTime() - PunchInDate.getTime()

            localStorage.removeItem('punchInHours')
            localStorage.removeItem('punchInMins')


            localStorage.removeItem('punchOutHours')
            localStorage.removeItem('punchOutMins')

            var data = { 'punchIn': PunchInDate, 'punchOut': PunchOutDate, 'diff': diff }

            this.punch.changePunchTimings(this.loggedUser, currentAttendence.id, data).subscribe(
                data => this.getAttendence(),
                error => console.log(error)
            )
        } else {
            var user = { 'userId': this.currentUser.id, 'id': this.loggedUser.id }

            this.punch.punchIn(user).subscribe(
                cbData => {
                    var date = new Date(new Date().getFullYear() + '-' + this.currentMonth + '-' + localStorage.getItem('date'))
                    var punchInHours = Number.parseInt(localStorage.getItem('punchInHours'))
                    var punchInMins = Number.parseInt(localStorage.getItem('punchInMins'))
                    var PunchInDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), punchInHours, punchInMins)

                    var punchOutHours = Number.parseInt(localStorage.getItem('punchOutHours'))
                    var punchOutMins = Number.parseInt(localStorage.getItem('punchOutMins'))
                    var PunchOutDate = new Date(date.getFullYear(), date.getMonth(), date.getDate(), punchOutHours, punchOutMins)

                    var NumDate = date.getTime();

                    var diff = PunchOutDate.getTime() - PunchInDate.getTime();
                    localStorage.removeItem('punchInHours')
                    localStorage.removeItem('punchInMins')


                    localStorage.removeItem('punchOutHours')
                    localStorage.removeItem('punchOutMins')

                    localStorage.removeItem('date')

                    var data = { 'punchIn': PunchInDate, 'punchOut': PunchOutDate, 'date': NumDate, 'diff': diff }

                    this.punch.changePunchTimings(this.loggedUser, (cbData.data).id, data).subscribe(
                        data => this.getAttendence(),
                        error => console.log(error)
                    )
                },
                error => console.log(error)
            )

        }

    }

    setLeave(currentAttendence) {
        localStorage.removeItem('leave');

        if (currentAttendence) {
            if (this.allPunchs) {
                for (var index = 0; index < this.allPunchs.length; index++) {



                    var data = { 'punchIn': null, 'punchOut': null, 'diff': null, 'reason': 'L' }

                    this.punch.changePunchTimings(this.loggedUser, ((this.allPunchs[index]).currentAttendence).id, data).subscribe(
                        data => window.location.reload(),
                        error => console.log(error)
                    )
                }
            } else {
                var data = { 'punchIn': null, 'punchOut': null, 'diff': null, 'reason': 'L' }

                this.punch.changePunchTimings(this.loggedUser, currentAttendence.id, data).subscribe(
                    data => window.location.reload(),
                    error => console.log(error)
                )
            }
        } else {
            var date = new Date(new Date().getFullYear() + '-' + this.currentMonth + '-' + localStorage.getItem('date')).getTime()
            var user = { 'userId': this.currentUser.id, 'id': this.loggedUser.id }

            var data1 = { 'punchIn': null, 'punchOut': null, 'date': date, 'reason': 'L', 'diff': null }

            this.punch.punchIn(user).subscribe(
                cbData => {
                    this.punch.changePunchTimings(this.loggedUser, (cbData.data).id, data1).subscribe(
                        data => window.location.reload(),
                        error => console.log(error)
                    )
                },
                error => console.log(error)
            )
        }
    }

    selectMonth(i) {
        this.currentMonth = this.monthNames[i];
        console.log(this.currentMonth)
        this.getMonthDays();
    }

    selectYear(i) {
        this.currentYear = i;
        this.getMonthDays();
    }

    showAnalytics(employee) {
        this.currentUser = employee;
        this.getAttendence();
        this.analytics = true;
        localStorage.setItem('showAnalytics', 'true');
    }

    hideAnalytics() {
        this.currentUser = null;
        this.analytics = false;
        localStorage.setItem('showAnalytics', 'false');
    }


    getAnalyticsStatus() {
        if (localStorage.getItem('showAnalytics') === 'true') {
            return true;
        }
        return false
    }


    download() {
        var csvData = this.ConvertToCSV(this.attendenceList);
        var a = document.createElement("a");
        a.setAttribute('style', 'display:none;');
        document.body.appendChild(a);
        var blob = new Blob([csvData], { type: 'text/csv' });
        var url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = this.currentUser.fullName + ' Export.csv';
        a.click();
    }

    ConvertToCSV(objArray) {
        var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
        var str = '';
        var row = "";

        for (var index in objArray[0]) {
            //Now convert each value to string and comma-separated
            row += index + ',';
        }
        row = row.slice(0, -1);
        //append Label row with line break
        str += row + '\r\n';

        for (var i = 0; i < array.length; i++) {
            var line = '';
            for (var index in array[i]) {
                if (line != '') line += ','

                line += array[i][index];
            }
            str += line + '\r\n';
        }
        return str;
    }

    csvData: any;
    readCsvData() {
        this.http.get('assets/' + this.currentUser.fullName + ' Export.csv')
            .subscribe(
            data => this.extractData(data),
            err => this.handleError(err)
            );
    }

    private extractData(res: any) {
        let csvData = res['_body'] || '';
        let allTextLines = csvData.split(/\r\n|\n/);
        let headers = allTextLines[0].split(',');
        let lines = [];

        for (let i = 1; i < allTextLines.length; i++) {
            // split content based on comma
            let data = allTextLines[i].split(',');
            if (data.length == headers.length) {
                let tarr = [];
                for (let j = 0; j < headers.length; j++) {
                    tarr.push(data[j]);
                }
                lines.push(tarr);
            }
        }
        for (var index = 0; index < lines.length; index++) {

            lines[index] = { "id": lines[index][0], "punchIn": lines[index][1], "punchOut": lines[index][2], "date": new Date(lines[index][3]), "reason": lines[index][4], "userId": lines[index][5], "updatedAt": new Date(lines[index][6]) }
        }
        this.csvData = lines;

        for (var index = 0; index < this.csvData.length; index++) {
            for (var j = 0; j < this.attendenceList.length; j++) {
                if ((this.csvData[index].date).getTime() === (this.attendenceList[j].date).getTime() && this.csvData[index].userId === this.attendenceList[j].userId && this.csvData[index].reason === this.attendenceList[j].reason) {
                    this.csvData.splice(index, 1);
                }
            }
        }

        for (var index = 0; index < this.csvData.length; index++) {
            this.csvImport(this.csvData[index])
        }
    }

    csvImport(object) {
        var user = { 'userId': object.userId, 'id': this.loggedUser.id }

        this.punch.punchIn(user).subscribe(
            cbData => {
                var PunchInDate = new Date(object.punchIn)

                var PunchOutDate = new Date(object.punchOut)

                
                var diff = PunchOutDate.getTime() - PunchInDate.getTime();

                var data = { 'punchIn': PunchInDate, 'punchOut': PunchOutDate, 'date': (object.date).getTime(), 'diff': diff }

                this.punch.changePunchTimings(this.loggedUser, (cbData.data).id, data).subscribe(
                    data => window.location.reload(),
                    error => console.log(error)
                )
            },
            error => console.log(error)
        )
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return errMsg;
    }



    // ------------------------------MonthlyChart------------------------------




    public monthChartData: Array<any> = [
        { data: [], label: 'Monthly Performance (Hours)' },
        // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
        // {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
    ];
    public monthChartLabels: Array<any> = [];
    public monthChartOptions: any = {
        responsive: true
    };
    public monthChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        // { // dark grey
        //   backgroundColor: 'rgba(77,83,96,0.2)',
        //   borderColor: 'rgba(77,83,96,1)',
        //   pointBackgroundColor: 'rgba(77,83,96,1)',
        //   pointBorderColor: '#fff',
        //   pointHoverBackgroundColor: '#fff',
        //   pointHoverBorderColor: 'rgba(77,83,96,1)'
        // },
        // { // grey
        //   backgroundColor: 'rgba(148,159,177,0.2)',
        //   borderColor: 'rgba(148,159,177,1)',
        //   pointBackgroundColor: 'rgba(148,159,177,1)',
        //   pointBorderColor: '#fff',
        //   pointHoverBackgroundColor: '#fff',
        //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        // }
    ];
    public monthChartLegend: boolean = true;
    public monthChartType: string = 'line';



    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }




    // ----------------------------WeeklyChart---------------------------------


    public weekChartData: Array<any> = [
        { data: [], label: 'Daily Stats (Hours)' },
        // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
        // {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
    ];
    public weekChartLabels: Array<any> = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    public weekChartOptions: any = {
        responsive: true
    };
    public weekChartColors: Array<any> = [
        // { // grey
        //   backgroundColor: 'rgba(148,159,177,0.2)',
        //   borderColor: 'rgba(148,159,177,1)',
        //   pointBackgroundColor: 'rgba(148,159,177,1)',
        //   pointBorderColor: '#fff',
        //   pointHoverBackgroundColor: '#fff',
        //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        // },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        // { // grey
        //   backgroundColor: 'rgba(148,159,177,0.2)',
        //   borderColor: 'rgba(148,159,177,1)',
        //   pointBackgroundColor: 'rgba(148,159,177,1)',
        //   pointBorderColor: '#fff',
        //   pointHoverBackgroundColor: '#fff',
        //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        // }
    ];
    public weekChartLegend: boolean = true;
    public weekChartType: string = 'line';

}
