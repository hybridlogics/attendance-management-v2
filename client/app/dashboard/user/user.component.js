"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var punch_service_1 = require("../punch.service");
var UserComponent = (function () {
    function UserComponent(navigator, service) {
        this.navigator = navigator;
        this.service = service;
        this.users = [];
        this.showForm = false;
        if (localStorage.getItem('userRole') !== 'admin') {
            this.navigator.navigateByUrl('/dashboard');
        }
    }
    UserComponent.prototype.ngOnInit = function () {
        this.loggedUser = JSON.parse(localStorage.getItem('my-app.id'));
        // $.getScript('../../../assets/js/material-dashboard.js');
        this.hideFormFunc();
        this.getUsers();
    };
    UserComponent.prototype.closeDeleteModal = function (answer) {
        var _this = this;
        $('#deleteModal').fadeOut();
        if (answer === 'Yes') {
            this.service.deleteUserbyId(this.loggedUser, this.currentUser.id).subscribe(function (data) {
                _this.users = data;
            }, function (error) { return console.log(error); });
            window.location.reload();
        }
    };
    UserComponent.prototype.displayDeleteModal = function (user) {
        this.currentUser = user;
        console.log(user);
        if (this.currentUser.id !== this.loggedUser.userId)
            $('#deleteModal').fadeIn();
    };
    UserComponent.prototype.showFormFunc = function (user) {
        this.currentUser = user;
        this.showForm = true;
        localStorage.setItem('showForm', 'true');
    };
    UserComponent.prototype.hideFormFunc = function () {
        this.currentUser = null;
        this.showForm = false;
        localStorage.setItem('showForm', 'false');
    };
    UserComponent.prototype.getShowFormStatus = function () {
        if (localStorage.getItem('showForm') === 'true') {
            return true;
        }
        return false;
    };
    UserComponent.prototype.getUsers = function () {
        var _this = this;
        this.service.getAllUsers(this.loggedUser).subscribe(function (data) {
            _this.users = data;
        }, function (error) { return console.log(error); });
    };
    UserComponent.prototype.updateProfile = function () {
        var _this = this;
        var data = { 'username': this.currentUser.username, 'email': this.currentUser.email, 'fullName': this.currentUser.fullName, 'userRole': this.currentUser.userRole, 'password': this.currentUser.password };
        this.service.updateUserbyId(data, this.loggedUser, this.currentUser.id).subscribe(function (data) { return _this.hideFormFunc(); }, function (error) { return console.log(error); });
    };
    UserComponent.prototype.changeUserRole = function (choice) {
        this.currentUser.userRole = choice;
    };
    UserComponent = __decorate([
        core_1.Component({
            selector: 'user-cmp',
            moduleId: module.id,
            templateUrl: 'user.component.html',
            styleUrls: ['user.component.css']
        }),
        __metadata("design:paramtypes", [router_1.Router, punch_service_1.PunchService])
    ], UserComponent);
    return UserComponent;
}());
exports.UserComponent = UserComponent;
//# sourceMappingURL=user.component.js.map