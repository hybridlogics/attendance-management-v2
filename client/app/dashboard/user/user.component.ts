import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { PunchService } from "../punch.service";


@Component({
    selector: 'user-cmp',
    moduleId: module.id,
    templateUrl: 'user.component.html',
    styleUrls: ['user.component.css']
})

export class UserComponent implements OnInit{

    currentUser: any;
    loggedUser: any;
    users=[]

    showForm: boolean = false;



    constructor(private navigator: Router, private service: PunchService) {
        if(localStorage.getItem('userRole') !== 'admin') {
            this.navigator.navigateByUrl('/dashboard');
        }
    }

    ngOnInit(){
        
        this.loggedUser = JSON.parse(localStorage.getItem('my-app.id'))

        // $.getScript('../../../assets/js/material-dashboard.js');
        this.hideFormFunc();
        this.getUsers();
    }

   closeDeleteModal(answer) {
        $('#deleteModal').fadeOut();
        if(answer === 'Yes') {
            this.service.deleteUserbyId(this.loggedUser, this.currentUser.id).subscribe(data => {
                this.users = data
            },
            error => console.log(error))
            
            window.location.reload()
        }
   }

   displayDeleteModal(user){
       this.currentUser = user;
       console.log(user)

       if(this.currentUser.id !== this.loggedUser.userId)
           $('#deleteModal').fadeIn();
   }

    showFormFunc(user) {
        this.currentUser = user;
        this.showForm = true;
        localStorage.setItem('showForm','true');
    }

    hideFormFunc() {
        this.currentUser = null;
        this.showForm = false;
        localStorage.setItem('showForm','false');
    }

    getShowFormStatus() {
        if(localStorage.getItem('showForm')==='true') {
            return true;
        }
        return false;
    }

    getUsers() {
        this.service.getAllUsers(this.loggedUser).subscribe(data => {
            this.users = data
        },
        error => console.log(error))
    }

    updateProfile(){
        
        var data = {'username': this.currentUser.username, 'email': this.currentUser.email, 'fullName': this.currentUser.fullName, 'userRole': this.currentUser.userRole, 'password': this.currentUser.password}

        this.service.updateUserbyId(data,this.loggedUser,this.currentUser.id).subscribe(
            data => this.hideFormFunc(),
            error => console.log(error)
        )
        
    }

    changeUserRole(choice) {
        this.currentUser.userRole = choice;
    }

}
