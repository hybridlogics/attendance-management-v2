import { Component, OnInit, trigger, state, style, transition, animate, OnDestroy } from '@angular/core';
import { Observable } from "rxjs/Observable";
import 'rxjs/add/observable/timer';
import { Subscription } from "rxjs/Subscription";
import { Router } from "@angular/router";
import { PunchService } from '../punch.service';

declare var $: any;

@Component({
    selector: 'home-cmp',
    moduleId: module.id,
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css']
})


export class HomeComponent implements OnInit, OnDestroy {
    users: any;
    ticks = 0;
    flag: boolean = false;
    currentDate: Date = new Date();
    currentMonth: any;
    currentYear: any;
    currentUser: any;
    fullName: string;
    isAdmin: boolean = false;


    punched: string = 'OUT';
    meridiem: any;
    punchedStatus: boolean = false;
    monthDays: any;

    private timer;
    // Subscription object
    private sub: Subscription;


    monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    daysName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
    years = [
        { name: '2017', id: 2017 },
        { name: '2018', id: 2018 },
        { name: '2019', id: 2019 },
        { name: '2020', id: 2020 },
        { name: '2021', id: 2021 },
        { name: '2022', id: 2021 }];
    // userMonthlyData= ['SAT', 'SUN', 'L', '08:17', '08:21', '08:55', '09:11', 'SAT', 'SUN', 'L', '08:41', '08:21', '00:00', '09:41', 'SAT', 'SUN', 'A', 'A', 'A', 'A', '-', 'SAT', 'SUN', '-', '-', '-', '-', '-', 'SAT', 'SUN', '-']
    userMonthlyData = [];
    weekHourSum = [0, 0, 0, 0, 0, 0, 0];
    attendenceList = [];
    allPunchs = [];
    OWD: any = 0;
    WD: any = 0;
    WH: any = 0;
    HW: any = 0;
    L: any = 0;
    A: any = 0;
    holidayStartDate: any = new Date().getFullYear() + '-' + (((new Date().getMonth() + 1).toString()).length === 1 ? '0' : '') + (new Date().getMonth() + 1) + '-' + new Date().getDate();
    holidayEndDate: any = new Date().getFullYear() + '-' + (((new Date().getMonth() + 1).toString()).length === 1 ? '0' : '') + (new Date().getMonth() + 1) + '-' + new Date().getDate();

    constructor(private punch: PunchService) { }


    ngAfterViewInit() {
        $('[data-toggle = "popover"]').popover();
    }


    ngOnInit() {
        if (localStorage.getItem('punchStatus') === 'IN') {
            this.punched = localStorage.getItem('punchStatus')
            this.punchedStatus = true;
        } else if (localStorage.getItem('punchStatus') === 'OUT') {
            this.punched = localStorage.getItem('punchStatus')
            this.punchedStatus = false;
        }

        this.currentYear = new Date().getFullYear();
        if (localStorage.getItem('i')) this.currentMonth = this.monthNames[Number.parseInt(localStorage.getItem('i'))]
        else this.currentMonth = this.monthNames[this.currentDate.getMonth()];

        localStorage.removeItem('i')
        // $('[data-toggle="checkbox"]').each(function () {
        //     if($(this).data('toggle') == 'switch') return;
        //
        //     var $checkbox = $(this);
        //     $checkbox.checkbox();
        // });


        this.timer = Observable.timer(2000, 1000);
        // subscribing to a observable returns a subscription object
        this.sub = this.timer.subscribe(t => this.tickerFunc(t));




        this.currentUser = JSON.parse(localStorage.getItem('my-app.id'));
        if (localStorage.getItem('userRole') === 'admin') {
            this.isAdmin = true;
        }

        this.fullName = localStorage.getItem('fullName');

        this.getAttendence();
        this.getUsers();

    }

    getAttendence() {
        var temp = 0;
        this.punch.getEmployeeAttendence(this.currentUser).subscribe(
            data => {
                for (var index = 0; index < data.length; index++) {
                    data[index].date = new Date(Number.parseInt(data[index].date))

                    if (data[index].userId === this.currentUser.userId) {
                        this.attendenceList.push(data[index]);

                        if (data[index].diff) {
                            for (var j = 0; j < this.attendenceList.length; j++) {
                                if ((data[index].date).getTime() === (this.attendenceList[j].date).getTime()) {
                                    if (temp === 1)
                                        this.attendenceList[j].diff = this.attendenceList[j].diff + data[index].diff;

                                    temp = 1;
                                }
                            }
                        }
                    }

                }
                this.getMonthDays();
            },
            error => console.log(error)
        )
    }

    index: number = 0;


    openPopover(target: HTMLElement, data): void {
        this.allPunchs = []
        for (var index = 0; index < this.attendenceList.length; index++) {
            var date = new Date(this.attendenceList[index].date);
            var punchIn = new Date((this.attendenceList[index]).punchIn);
            var punchOut = new Date((this.attendenceList[index]).punchOut);
            if (this.attendenceList[index].reason) {
                var punchInHours = '-';
                var punchOutHours = '-';
            } else if (data.date === date.getDate()) {
                var punchInHours = (((punchIn.getHours()).toString()).length === 1 ? '0' : '') + punchIn.getHours() + ':' + (((punchIn.getMinutes()).toString()).length === 1 ? '0' : '') + punchIn.getMinutes() + ':' + (((punchIn.getSeconds()).toString()).length === 1 ? '0' : '') + punchIn.getSeconds()
                var punchOutHours = (((punchOut.getHours()).toString()).length === 1 ? '0' : '') + punchOut.getHours() + ':' + (((punchOut.getMinutes()).toString()).length === 1 ? '0' : '') + punchOut.getMinutes() + ':' + (((punchOut.getSeconds()).toString()).length === 1 ? '0' : '') + punchOut.getSeconds()

                this.allPunchs.push({ 'punchIn': punchInHours, 'punchOut': punchOutHours })
            } else {
                this.allPunchs = []
                var punchInHours = '-';
                var punchOutHours = '-';
            }
        }

        for (var index = 0; index < this.allPunchs.length; index++) {
            for (var j = 0; j < this.allPunchs.length; j++) {
                if (this.allPunchs[index].punchIn === this.allPunchs[j].punchIn && this.allPunchs[index].punchOut === this.allPunchs[j].punchOut) {
                    this.allPunchs.splice(index, 1);
                }

            }

        }

        var myContent = '';
        for (var index = 0; index < this.allPunchs.length; index++) {
            myContent = 'Punched In: ' + this.allPunchs[index].punchIn + '<br>Punched Out: ' + this.allPunchs[index].punchOut + '<br><hr>' + myContent

        }
        $(target).popover({
            html: true,
            // title : '<a class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true" style="color:black"></i></a>',
            content: '<div style="max-height:200px;overflow-y:scroll"><h6><b>' + this.currentMonth + ', ' + data.date + '&nbsp;&nbsp;&nbsp;' + this.daysName[(new Date(this.currentYear + '-' + this.currentMonth + '-' + data.date)).getDay()] + '</b></h6>' + myContent + '</div>'
        })
        $(document).on("click", ".popover .close", function () {
            $(this).parents(".popover").popover('hide');
        });
    }

    getMonthDays() {
        var year = this.currentYear;
        var month = this.monthNames.indexOf(this.currentMonth);
        this.monthDays = new Date(year, month + 1, 0).getDate()


        var myDate = new Date();
        myDate.setFullYear(year);
        myDate.setMonth(month);


        this.userMonthlyData = [];
        var monthHours = [];
        this.OWD = 0;
        this.WD = 0;
        this.WH = 0;
        this.HW = 0;
        this.L = 0;
        this.A = 0;
        this.index = 0;

        var date = new Date(1990);
        var absentDate = []

        for (var index = 1; index <= this.monthDays; index++) {



            this.monthChartLabels.push(index);

            for (this.index = 0; this.index < this.attendenceList.length; this.index++) {
                date = new Date((this.attendenceList[this.index]).date)
                if (index === date.getDate() && date.getMonth() === myDate.getMonth() && date.getFullYear() === myDate.getFullYear()) {
                    break;
                }

            }


            myDate.setDate(index);
            if (myDate.getDay() == 6) {
                this.userMonthlyData.push({ day: 'SAT', date: index });
                monthHours.push(0)
            }
            else if (myDate.getDay() == 0) {
                this.userMonthlyData.push({ day: 'SUN', date: index });
                monthHours.push(0)
            }
            else if (date && index === date.getDate() && date.getMonth() === myDate.getMonth() && date.getFullYear() === myDate.getFullYear()) {



                if ((this.attendenceList[this.index]).reason) {


                    if ((this.attendenceList[this.index]).reason === 'P') {
                        this.WD++;

                        if (myDate.getDay() == 1) {
                            this.weekHourSum = [0, 0, 0, 0, 0, 0, 0];
                        }

                        // var punchIn = new Date((this.attendenceList[this.index]).punchIn).getTime();
                        // var punchOut = new Date((this.attendenceList[this.index]).punchOut).getTime();

                        // var diff = punchOut - punchIn;
                        var diffDate = new Date((this.attendenceList[this.index]).diff);
                        var sec = diffDate.getSeconds();
                        var min = diffDate.getMinutes();
                        var hours = diffDate.getHours() - 5;
                        var totalHours = hours + min / 60 + sec / 3600;

                        this.weekHourSum[myDate.getDay()] = this.weekHourSum[myDate.getDay()] + Number.parseFloat(totalHours.toFixed(4));
                        monthHours.push(this.weekHourSum[myDate.getDay()]);

                        this.userMonthlyData.push({ day: ((hours.toString()).length === 2 ? '' : '0') + hours + ':' + ((min.toString()).length === 2 ? '' : '0') + min + ':' + ((sec.toString()).length === 2 ? '' : '0') + sec, date: index })

                    }



                    else if ((this.attendenceList[this.index]).reason === 'L') {
                        this.L++;
                        this.userMonthlyData.push({ day: (this.attendenceList[this.index]).reason, date: index });
                        monthHours.push(0)
                    }
                    else if ((this.attendenceList[this.index]).reason === 'A') {
                        this.A++;
                        this.userMonthlyData.push({ day: (this.attendenceList[this.index]).reason, date: index });
                        monthHours.push(0)
                    }
                    else if ((this.attendenceList[this.index]).reason === 'H') {
                        this.userMonthlyData.push({ day: (this.attendenceList[this.index]).reason, date: index });
                        monthHours.push(0)
                    }

                }

                if (this.currentDate.getDate() >= index)
                    this.OWD++;

            }
            else {
                if (this.currentDate.getDate() > index && myDate.getMonth() === new Date().getMonth()) {
                    this.A++;
                    this.OWD++;
                    this.userMonthlyData.push({ day: 'A', date: index });
                    absentDate.push(index)
                } else {
                    this.userMonthlyData.push({ day: '-', date: index });
                }

                monthHours.push(0)
            }

        }

        for (var index = 0; index < absentDate.length; index++) {
            this.markAbsent(absentDate[index])

        }

        for (var index = 0; index < monthHours.length; index++) {
            this.HW = this.HW + monthHours[index];

        }
        this.HW = Number.parseFloat(this.HW).toFixed(2);

        this.WH = Number.parseFloat(this.OWD * 9 + '').toFixed(2);

        this.monthChartData[0] = { data: monthHours, label: 'Monthly Performance (Hours) of ' + this.currentMonth + ' ' + this.currentYear };
        if (localStorage.getItem('currentWeekData')) {
            var res = localStorage.getItem('currentWeekData').split(',');
            this.weekChartData[0].data = res

        }
        else this.weekChartData[0].data = this.weekHourSum;

        // localStorage.removeItem('currentWeekData')
        if (this.OWD < 0) {
            this.OWD = 0;
            this.WH = 0;
        }
        this.flag = true;
        // localStorage.removeItem('currentWeekData')
    }
    markAbsent(index) {
        var date = new Date(this.currentYear + '-' + this.currentMonth + '-' + index).getTime()
        var user = { 'userId': this.currentUser.userId, 'id': this.currentUser.id }
        var data = { 'punchIn': null, 'punchOut': null, 'date': date, 'reason': 'A' }

        this.punch.punchIn(user).subscribe(
            cbData => {
                this.punch.changePunchTimings(this.currentUser, (cbData.data).id, data).subscribe(
                    data => console.log(''),
                    error => console.log(error)
                )
            },
            error => console.log(error)
        )


    }

    tickerFunc(tick) {
        this.clock();
        this.ticks = tick
    }

    ngOnDestroy() {
        localStorage.removeItem('currentWeekData')

    }

    selectMonth(i) {
        localStorage.setItem('i', i)
        localStorage.setItem('currentWeekData', this.weekChartData[0].data)
        window.location.reload()
        this.getMonthDays();
    }

    selectYear(i) {
        this.currentYear = i;
        this.getMonthDays();
    }



    punchFunc(str) {
        if (str === 'in') {
            var flag = 1;
            this.punched = "IN";
            this.punchedStatus = true;
            localStorage.setItem('punchStatus', 'IN')
            this.punch.getEmployeeAttendence(this.currentUser).subscribe(
                data => {
                    for (var index = 0; index < data.length; index++) {
                        // console.log(data[index].date)
                        if (this.currentUser.userId === data[index].userId && new Date().getDate() === new Date(data[index].date).getDate() && data[index].reason != "P") {
                           
                                flag = 0;
                                var data1 = { 'punchIn': new Date(), 'punchOut': null, 'date': new Date(), 'reason': 'P' }


                                this.punch.changePunchTimings(this.currentUser, data[index].id, data1).subscribe(
                                    data => {
                                        localStorage.setItem('punchIn', data.data.punchIn);
                                        window.location.reload();
                                    },
                                    error => console.log(error)
                                )
                            
                        }
                    }
                    if (flag) {
                        this.punch.punchIn(this.currentUser).subscribe(
                            data => localStorage.setItem('punchIn', data.data.punchIn),
                            error => console.log(error)
                        );
                    }
                },
                error => console.log(error)
            )
            
        }
        else {
            this.punched = "OUT";
            this.punchedStatus = false;
            localStorage.setItem('punchStatus', 'OUT')
            this.punch.punchOut(this.currentUser, new Date(localStorage.getItem('punchIn')).getTime()).subscribe(
                data => console.log('Punched Out: ' + data),
                error => console.log(error)
            );
            localStorage.removeItem('punchIn')
            window.location.reload()
        }
    }

    getUsers() {
        this.punch.getAllUsers(this.currentUser).subscribe(data => {
            this.users = data

        },
            error => console.log(error))
    }

    setHoliday(date, currentUser) {
        var flag = 1;
        this.punch.getEmployeeAttendence(this.currentUser).subscribe(
            data => {
                for (var index = 0; index < data.length; index++) {
                    // console.log(data[index].date)
                    if (currentUser.id === data[index].userId && date === Number.parseInt(data[index].date)) {

                        if (data[index].reason === "P") {
                            flag = 0;
                        } else {
                            flag = 0;
                            var data1 = { 'punchIn': null, 'punchOut': null, 'date': data[index].date, 'reason': 'H' }


                            this.punch.changePunchTimings(this.currentUser, data[index].id, data1).subscribe(
                                data => window.location.reload(),
                                error => console.log(error)
                            )
                        }

                    }
                }

                if (flag) {
                    var user = { 'userId': currentUser.id, 'id': this.currentUser.id }

                    var data1 = { 'punchIn': null, 'punchOut': null, 'date': date, 'reason': 'H' }

                    this.punch.punchIn(user).subscribe(
                        cbData => {
                            this.punch.changePunchTimings(this.currentUser, (cbData.data).id, data1).subscribe(
                                data => window.location.reload(),
                                error => console.log(error)
                            )
                        },
                        error => console.log(error)
                    )
                }
            },
            error => console.log(error)
        )

        // var user = { 'userId': currentUser.id, 'id': this.currentUser.id }

        // var data = { 'punchIn': null, 'punchOut': null, 'date': date, 'reason': 'H' }

        // this.punch.punchIn(user).subscribe(
        //     cbData => {
        //         this.punch.changePunchTimings(this.currentUser, (cbData.data).id, data).subscribe(
        //             data => window.location.reload(),
        //             error => console.log(error)
        //         )
        //     },
        //     error => console.log(error)
        // )
    }

    cancelHoliday(date, currentUser) {
        console.log(date)
        this.punch.getEmployeeAttendence(this.currentUser).subscribe(
            data => {

                for (var index = 0; index < data.length; index++) {
                    // console.log(data[index].date)
                    if (this.currentUser.userId === data[index].userId)
                        if (date === Number.parseInt(data[index].date)) {

                            var data1 = { 'date': date, 'reason': null }


                            this.punch.changePunchTimings(this.currentUser, data[index].id, data1).subscribe(
                                data => window.location.reload(),
                                error => console.log(error)
                            )

                        }

                }
            },
            error => console.log(error)
        )
    }

    closeHolidayModal(answer) {
        $('#holidayModal').fadeOut();

        if (answer === 'save' && this.modaltype === 'declare') {
            this.holidayStartDate = new Date(this.holidayStartDate + '')
            this.holidayEndDate = new Date(this.holidayEndDate + '')

            // var dates = [];
            // var month = this.holidayStartDate.getMonth();
            // var year = this.holidayStartDate.getFullYear();

            for (var index = this.holidayStartDate.getDate(); index <= this.holidayEndDate.getDate(); index++) {


                for (var j = 0; j < this.users.length; j++) {
                    this.setHoliday(new Date(this.holidayStartDate.getFullYear() + '-' + (this.holidayStartDate.getMonth() + 1) + '-' + index).getTime(), this.users[j])

                }
            }

        } if (answer === 'save' && this.modaltype === 'cancel') {
            this.holidayStartDate = new Date(this.holidayStartDate + '')
            this.holidayEndDate = new Date(this.holidayEndDate + '')

            // var dates = [];
            // var month = this.holidayStartDate.getMonth();
            // var year = this.holidayStartDate.getFullYear();

            // for (var index = this.holidayStartDate.getDate(); index <= this.holidayEndDate.getDate(); index++) {


            // for (var j = 0; j < 1; j++) {
            // if (this.users.id === this.currentUser.id)
            // this.cancelHoliday(new Date(this.holidayStartDate.getFullYear() + '-' + (this.holidayStartDate.getMonth() + 1) + '-' + this.holidayStartDate.getDate()).getTime(), this.users[j])

            // }
            // }
        }
    }
    modaltype: string;
    displayHolidayModal(type) {
        this.modaltype = type;
        $('#holidayModal').fadeIn();
    }


    clock() {

        //Save the times in variables
        var today = new Date();

        var hours = today.getHours();
        var minutes = today.getMinutes();
        var seconds = today.getSeconds();
        var minString;
        var secString;

        //Set the AM or PM time

        if (hours >= 12) {
            this.meridiem = " PM";
        }
        else {
            this.meridiem = " AM";
        }


        //convert hours to 12 hour format and put 0 in front
        if (hours > 12) {
            hours = hours - 12;
        }
        else if (hours === 0) {
            hours = 12;
        }

        //Put 0 in front of single digit minutes and seconds

        if (minutes < 10) {
            minString = "0" + minutes;
        }
        else {
            minString = minutes;
        }

        if (seconds < 10) {
            secString = "0" + seconds;
        }
        else {
            secString = seconds;
        }
        document.getElementById("clock").innerHTML = (hours + ":" + minString + ":" + secString + this.meridiem);

    }




    // ------------------------------MonthlyChart------------------------------


    public monthChartData: Array<any> = [

    ];
    public monthChartLabels: Array<any> = [];
    public monthChartOptions: any = {
        responsive: true
    };
    public monthChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        // { // dark grey
        //   backgroundColor: 'rgba(77,83,96,0.2)',
        //   borderColor: 'rgba(77,83,96,1)',
        //   pointBackgroundColor: 'rgba(77,83,96,1)',
        //   pointBorderColor: '#fff',
        //   pointHoverBackgroundColor: '#fff',
        //   pointHoverBorderColor: 'rgba(77,83,96,1)'
        // },
        // { // grey
        //   backgroundColor: 'rgba(148,159,177,0.2)',
        //   borderColor: 'rgba(148,159,177,1)',
        //   pointBackgroundColor: 'rgba(148,159,177,1)',
        //   pointBorderColor: '#fff',
        //   pointHoverBackgroundColor: '#fff',
        //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        // }
    ];
    public monthChartLegend: boolean = true;
    public monthChartType: string = 'line';



    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }

    makeMonthlyChart() {

    }




    // ----------------------------WeeklyChart---------------------------------


    public weekChartData: Array<any> = [
        { data: [], label: 'Current Week Stats (Hours)' },
        // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'},
        // {data: [18, 48, 77, 9, 100, 27, 40], label: 'Series C'}
    ];
    public weekChartLabels: Array<any> = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    public weekChartOptions: any = {
        responsive: true
    };
    public weekChartColors: Array<any> = [
        // { // grey
        //   backgroundColor: 'rgba(148,159,177,0.2)',
        //   borderColor: 'rgba(148,159,177,1)',
        //   pointBackgroundColor: 'rgba(148,159,177,1)',
        //   pointBorderColor: '#fff',
        //   pointHoverBackgroundColor: '#fff',
        //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        // },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        // { // grey
        //   backgroundColor: 'rgba(148,159,177,0.2)',
        //   borderColor: 'rgba(148,159,177,1)',
        //   pointBackgroundColor: 'rgba(148,159,177,1)',
        //   pointBorderColor: '#fff',
        //   pointHoverBackgroundColor: '#fff',
        //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        // }
    ];
    public weekChartLegend: boolean = true;
    public weekChartType: string = 'line';



}
