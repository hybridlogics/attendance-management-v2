"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/timer");
var punch_service_1 = require("../punch.service");
var HomeComponent = (function () {
    function HomeComponent(punch) {
        this.punch = punch;
        this.ticks = 0;
        this.flag = false;
        this.currentDate = new Date();
        this.isAdmin = false;
        this.punched = 'OUT';
        this.punchedStatus = false;
        this.monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        this.daysName = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        this.years = [
            { name: '2017', id: 2017 },
            { name: '2018', id: 2018 },
            { name: '2019', id: 2019 },
            { name: '2020', id: 2020 },
            { name: '2021', id: 2021 },
            { name: '2022', id: 2021 }
        ];
        // userMonthlyData= ['SAT', 'SUN', 'L', '08:17', '08:21', '08:55', '09:11', 'SAT', 'SUN', 'L', '08:41', '08:21', '00:00', '09:41', 'SAT', 'SUN', 'A', 'A', 'A', 'A', '-', 'SAT', 'SUN', '-', '-', '-', '-', '-', 'SAT', 'SUN', '-']
        this.userMonthlyData = [];
        this.weekHourSum = [0, 0, 0, 0, 0, 0, 0];
        this.attendenceList = [];
        this.allPunchs = [];
        this.OWD = 0;
        this.WD = 0;
        this.WH = 0;
        this.HW = 0;
        this.L = 0;
        this.A = 0;
        this.holidayStartDate = new Date().getFullYear() + '-' + (((new Date().getMonth() + 1).toString()).length === 1 ? '0' : '') + (new Date().getMonth() + 1) + '-' + new Date().getDate();
        this.holidayEndDate = new Date().getFullYear() + '-' + (((new Date().getMonth() + 1).toString()).length === 1 ? '0' : '') + (new Date().getMonth() + 1) + '-' + new Date().getDate();
        this.index = 0;
        // ------------------------------MonthlyChart------------------------------
        this.monthChartData = [];
        this.monthChartLabels = [];
        this.monthChartOptions = {
            responsive: true
        };
        this.monthChartColors = [
            {
                backgroundColor: 'rgba(148,159,177,0.2)',
                borderColor: 'rgba(148,159,177,1)',
                pointBackgroundColor: 'rgba(148,159,177,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            },
        ];
        this.monthChartLegend = true;
        this.monthChartType = 'line';
        // ----------------------------WeeklyChart---------------------------------
        this.weekChartData = [
            { data: [], label: 'Current Week Stats (Hours)' },
        ];
        this.weekChartLabels = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        this.weekChartOptions = {
            responsive: true
        };
        this.weekChartColors = [
            // { // grey
            //   backgroundColor: 'rgba(148,159,177,0.2)',
            //   borderColor: 'rgba(148,159,177,1)',
            //   pointBackgroundColor: 'rgba(148,159,177,1)',
            //   pointBorderColor: '#fff',
            //   pointHoverBackgroundColor: '#fff',
            //   pointHoverBorderColor: 'rgba(148,159,177,0.8)'
            // },
            {
                backgroundColor: 'rgba(77,83,96,0.2)',
                borderColor: 'rgba(77,83,96,1)',
                pointBackgroundColor: 'rgba(77,83,96,1)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgba(77,83,96,1)'
            },
        ];
        this.weekChartLegend = true;
        this.weekChartType = 'line';
    }
    HomeComponent.prototype.ngAfterViewInit = function () {
        $('[data-toggle = "popover"]').popover();
    };
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem('punchStatus') === 'IN') {
            this.punched = localStorage.getItem('punchStatus');
            this.punchedStatus = true;
        }
        else if (localStorage.getItem('punchStatus') === 'OUT') {
            this.punched = localStorage.getItem('punchStatus');
            this.punchedStatus = false;
        }
        this.currentYear = new Date().getFullYear();
        if (localStorage.getItem('i'))
            this.currentMonth = this.monthNames[Number.parseInt(localStorage.getItem('i'))];
        else
            this.currentMonth = this.monthNames[this.currentDate.getMonth()];
        localStorage.removeItem('i');
        // $('[data-toggle="checkbox"]').each(function () {
        //     if($(this).data('toggle') == 'switch') return;
        //
        //     var $checkbox = $(this);
        //     $checkbox.checkbox();
        // });
        this.timer = Observable_1.Observable.timer(2000, 1000);
        // subscribing to a observable returns a subscription object
        this.sub = this.timer.subscribe(function (t) { return _this.tickerFunc(t); });
        this.currentUser = JSON.parse(localStorage.getItem('my-app.id'));
        if (localStorage.getItem('userRole') === 'admin') {
            this.isAdmin = true;
        }
        this.fullName = localStorage.getItem('fullName');
        this.getAttendence();
        this.getUsers();
    };
    HomeComponent.prototype.getAttendence = function () {
        var _this = this;
        var temp = 0;
        this.punch.getEmployeeAttendence(this.currentUser).subscribe(function (data) {
            for (var index = 0; index < data.length; index++) {
                data[index].date = new Date(Number.parseInt(data[index].date));
                if (data[index].userId === _this.currentUser.userId) {
                    _this.attendenceList.push(data[index]);
                    if (data[index].diff) {
                        for (var j = 0; j < _this.attendenceList.length; j++) {
                            if ((data[index].date).getTime() === (_this.attendenceList[j].date).getTime()) {
                                if (temp === 1)
                                    _this.attendenceList[j].diff = _this.attendenceList[j].diff + data[index].diff;
                                temp = 1;
                            }
                        }
                    }
                }
            }
            _this.getMonthDays();
        }, function (error) { return console.log(error); });
    };
    HomeComponent.prototype.openPopover = function (target, data) {
        this.allPunchs = [];
        for (var index = 0; index < this.attendenceList.length; index++) {
            var date = new Date(this.attendenceList[index].date);
            var punchIn = new Date((this.attendenceList[index]).punchIn);
            var punchOut = new Date((this.attendenceList[index]).punchOut);
            if (this.attendenceList[index].reason) {
                var punchInHours = '-';
                var punchOutHours = '-';
            }
            else if (data.date === date.getDate()) {
                var punchInHours = (((punchIn.getHours()).toString()).length === 1 ? '0' : '') + punchIn.getHours() + ':' + (((punchIn.getMinutes()).toString()).length === 1 ? '0' : '') + punchIn.getMinutes() + ':' + (((punchIn.getSeconds()).toString()).length === 1 ? '0' : '') + punchIn.getSeconds();
                var punchOutHours = (((punchOut.getHours()).toString()).length === 1 ? '0' : '') + punchOut.getHours() + ':' + (((punchOut.getMinutes()).toString()).length === 1 ? '0' : '') + punchOut.getMinutes() + ':' + (((punchOut.getSeconds()).toString()).length === 1 ? '0' : '') + punchOut.getSeconds();
                this.allPunchs.push({ 'punchIn': punchInHours, 'punchOut': punchOutHours });
            }
            else {
                this.allPunchs = [];
                var punchInHours = '-';
                var punchOutHours = '-';
            }
        }
        for (var index = 0; index < this.allPunchs.length; index++) {
            for (var j = 0; j < this.allPunchs.length; j++) {
                if (this.allPunchs[index].punchIn === this.allPunchs[j].punchIn && this.allPunchs[index].punchOut === this.allPunchs[j].punchOut) {
                    this.allPunchs.splice(index, 1);
                }
            }
        }
        var myContent = '';
        for (var index = 0; index < this.allPunchs.length; index++) {
            myContent = 'Punched In: ' + this.allPunchs[index].punchIn + '<br>Punched Out: ' + this.allPunchs[index].punchOut + '<br><hr>' + myContent;
        }
        $(target).popover({
            html: true,
            // title : '<a class="close" data-dismiss="alert"><i class="fa fa-times" aria-hidden="true" style="color:black"></i></a>',
            content: '<div style="max-height:200px;overflow-y:scroll"><h6><b>' + this.currentMonth + ', ' + data.date + '&nbsp;&nbsp;&nbsp;' + this.daysName[(new Date(this.currentYear + '-' + this.currentMonth + '-' + data.date)).getDay()] + '</b></h6>' + myContent + '</div>'
        });
        $(document).on("click", ".popover .close", function () {
            $(this).parents(".popover").popover('hide');
        });
    };
    HomeComponent.prototype.getMonthDays = function () {
        var year = this.currentYear;
        var month = this.monthNames.indexOf(this.currentMonth);
        this.monthDays = new Date(year, month + 1, 0).getDate();
        var myDate = new Date();
        myDate.setFullYear(year);
        myDate.setMonth(month);
        this.userMonthlyData = [];
        var monthHours = [];
        this.OWD = 0;
        this.WD = 0;
        this.WH = 0;
        this.HW = 0;
        this.L = 0;
        this.A = 0;
        this.index = 0;
        var date = new Date(1990);
        var absentDate = [];
        for (var index = 1; index <= this.monthDays; index++) {
            this.monthChartLabels.push(index);
            for (this.index = 0; this.index < this.attendenceList.length; this.index++) {
                date = new Date((this.attendenceList[this.index]).date);
                if (index === date.getDate() && date.getMonth() === myDate.getMonth() && date.getFullYear() === myDate.getFullYear()) {
                    break;
                }
            }
            myDate.setDate(index);
            if (myDate.getDay() == 6) {
                this.userMonthlyData.push({ day: 'SAT', date: index });
                monthHours.push(0);
            }
            else if (myDate.getDay() == 0) {
                this.userMonthlyData.push({ day: 'SUN', date: index });
                monthHours.push(0);
            }
            else if (date && index === date.getDate() && date.getMonth() === myDate.getMonth() && date.getFullYear() === myDate.getFullYear()) {
                if ((this.attendenceList[this.index]).reason) {
                    if ((this.attendenceList[this.index]).reason === 'P') {
                        this.WD++;
                        if (myDate.getDay() == 1) {
                            this.weekHourSum = [0, 0, 0, 0, 0, 0, 0];
                        }
                        // var punchIn = new Date((this.attendenceList[this.index]).punchIn).getTime();
                        // var punchOut = new Date((this.attendenceList[this.index]).punchOut).getTime();
                        // var diff = punchOut - punchIn;
                        var diffDate = new Date((this.attendenceList[this.index]).diff);
                        var sec = diffDate.getSeconds();
                        var min = diffDate.getMinutes();
                        var hours = diffDate.getHours() - 5;
                        var totalHours = hours + min / 60 + sec / 3600;
                        this.weekHourSum[myDate.getDay()] = this.weekHourSum[myDate.getDay()] + Number.parseFloat(totalHours.toFixed(4));
                        monthHours.push(this.weekHourSum[myDate.getDay()]);
                        this.userMonthlyData.push({ day: ((hours.toString()).length === 2 ? '' : '0') + hours + ':' + ((min.toString()).length === 2 ? '' : '0') + min + ':' + ((sec.toString()).length === 2 ? '' : '0') + sec, date: index });
                    }
                    else if ((this.attendenceList[this.index]).reason === 'L') {
                        this.L++;
                        this.userMonthlyData.push({ day: (this.attendenceList[this.index]).reason, date: index });
                        monthHours.push(0);
                    }
                    else if ((this.attendenceList[this.index]).reason === 'A') {
                        this.A++;
                        this.userMonthlyData.push({ day: (this.attendenceList[this.index]).reason, date: index });
                        monthHours.push(0);
                    }
                    else if ((this.attendenceList[this.index]).reason === 'H') {
                        this.userMonthlyData.push({ day: (this.attendenceList[this.index]).reason, date: index });
                        monthHours.push(0);
                    }
                }
                if (this.currentDate.getDate() >= index)
                    this.OWD++;
            }
            else {
                if (this.currentDate.getDate() > index && myDate.getMonth() === new Date().getMonth()) {
                    this.A++;
                    this.OWD++;
                    this.userMonthlyData.push({ day: 'A', date: index });
                    absentDate.push(index);
                }
                else {
                    this.userMonthlyData.push({ day: '-', date: index });
                }
                monthHours.push(0);
            }
        }
        for (var index = 0; index < absentDate.length; index++) {
            this.markAbsent(absentDate[index]);
        }
        for (var index = 0; index < monthHours.length; index++) {
            this.HW = this.HW + monthHours[index];
        }
        this.HW = Number.parseFloat(this.HW).toFixed(2);
        this.WH = Number.parseFloat(this.OWD * 9 + '').toFixed(2);
        this.monthChartData[0] = { data: monthHours, label: 'Monthly Performance (Hours) of ' + this.currentMonth + ' ' + this.currentYear };
        if (localStorage.getItem('currentWeekData')) {
            var res = localStorage.getItem('currentWeekData').split(',');
            this.weekChartData[0].data = res;
        }
        else
            this.weekChartData[0].data = this.weekHourSum;
        // localStorage.removeItem('currentWeekData')
        if (this.OWD < 0) {
            this.OWD = 0;
            this.WH = 0;
        }
        this.flag = true;
        // localStorage.removeItem('currentWeekData')
    };
    HomeComponent.prototype.markAbsent = function (index) {
        var _this = this;
        var date = new Date(this.currentYear + '-' + this.currentMonth + '-' + index).getTime();
        var user = { 'userId': this.currentUser.userId, 'id': this.currentUser.id };
        var data = { 'punchIn': null, 'punchOut': null, 'date': date, 'reason': 'A' };
        this.punch.punchIn(user).subscribe(function (cbData) {
            _this.punch.changePunchTimings(_this.currentUser, (cbData.data).id, data).subscribe(function (data) { return console.log(''); }, function (error) { return console.log(error); });
        }, function (error) { return console.log(error); });
    };
    HomeComponent.prototype.tickerFunc = function (tick) {
        this.clock();
        this.ticks = tick;
    };
    HomeComponent.prototype.ngOnDestroy = function () {
        localStorage.removeItem('currentWeekData');
    };
    HomeComponent.prototype.selectMonth = function (i) {
        localStorage.setItem('i', i);
        localStorage.setItem('currentWeekData', this.weekChartData[0].data);
        window.location.reload();
        this.getMonthDays();
    };
    HomeComponent.prototype.selectYear = function (i) {
        this.currentYear = i;
        this.getMonthDays();
    };
    HomeComponent.prototype.punchFunc = function (str) {
        var _this = this;
        if (str === 'in') {
            var flag = 1;
            this.punched = "IN";
            this.punchedStatus = true;
            localStorage.setItem('punchStatus', 'IN');
            this.punch.getEmployeeAttendence(this.currentUser).subscribe(function (data) {
                for (var index = 0; index < data.length; index++) {
                    // console.log(data[index].date)
                    if (_this.currentUser.userId === data[index].userId && new Date().getDate() === new Date(data[index].date).getDate() && data[index].reason != "P") {
                        flag = 0;
                        var data1 = { 'punchIn': new Date(), 'punchOut': null, 'date': new Date(), 'reason': 'P' };
                        _this.punch.changePunchTimings(_this.currentUser, data[index].id, data1).subscribe(function (data) {
                            localStorage.setItem('punchIn', data.data.punchIn);
                            window.location.reload();
                        }, function (error) { return console.log(error); });
                    }
                }
                if (flag) {
                    _this.punch.punchIn(_this.currentUser).subscribe(function (data) { return localStorage.setItem('punchIn', data.data.punchIn); }, function (error) { return console.log(error); });
                }
            }, function (error) { return console.log(error); });
        }
        else {
            this.punched = "OUT";
            this.punchedStatus = false;
            localStorage.setItem('punchStatus', 'OUT');
            this.punch.punchOut(this.currentUser, new Date(localStorage.getItem('punchIn')).getTime()).subscribe(function (data) { return console.log('Punched Out: ' + data); }, function (error) { return console.log(error); });
            localStorage.removeItem('punchIn');
            window.location.reload();
        }
    };
    HomeComponent.prototype.getUsers = function () {
        var _this = this;
        this.punch.getAllUsers(this.currentUser).subscribe(function (data) {
            _this.users = data;
        }, function (error) { return console.log(error); });
    };
    HomeComponent.prototype.setHoliday = function (date, currentUser) {
        var _this = this;
        var flag = 1;
        this.punch.getEmployeeAttendence(this.currentUser).subscribe(function (data) {
            for (var index = 0; index < data.length; index++) {
                // console.log(data[index].date)
                if (currentUser.id === data[index].userId && date === Number.parseInt(data[index].date)) {
                    if (data[index].reason === "P") {
                        flag = 0;
                    }
                    else {
                        flag = 0;
                        var data1 = { 'punchIn': null, 'punchOut': null, 'date': data[index].date, 'reason': 'H' };
                        _this.punch.changePunchTimings(_this.currentUser, data[index].id, data1).subscribe(function (data) { return window.location.reload(); }, function (error) { return console.log(error); });
                    }
                }
            }
            if (flag) {
                var user = { 'userId': currentUser.id, 'id': _this.currentUser.id };
                var data1 = { 'punchIn': null, 'punchOut': null, 'date': date, 'reason': 'H' };
                _this.punch.punchIn(user).subscribe(function (cbData) {
                    _this.punch.changePunchTimings(_this.currentUser, (cbData.data).id, data1).subscribe(function (data) { return window.location.reload(); }, function (error) { return console.log(error); });
                }, function (error) { return console.log(error); });
            }
        }, function (error) { return console.log(error); });
        // var user = { 'userId': currentUser.id, 'id': this.currentUser.id }
        // var data = { 'punchIn': null, 'punchOut': null, 'date': date, 'reason': 'H' }
        // this.punch.punchIn(user).subscribe(
        //     cbData => {
        //         this.punch.changePunchTimings(this.currentUser, (cbData.data).id, data).subscribe(
        //             data => window.location.reload(),
        //             error => console.log(error)
        //         )
        //     },
        //     error => console.log(error)
        // )
    };
    HomeComponent.prototype.cancelHoliday = function (date, currentUser) {
        var _this = this;
        console.log(date);
        this.punch.getEmployeeAttendence(this.currentUser).subscribe(function (data) {
            for (var index = 0; index < data.length; index++) {
                // console.log(data[index].date)
                if (_this.currentUser.userId === data[index].userId)
                    if (date === Number.parseInt(data[index].date)) {
                        var data1 = { 'date': date, 'reason': null };
                        _this.punch.changePunchTimings(_this.currentUser, data[index].id, data1).subscribe(function (data) { return window.location.reload(); }, function (error) { return console.log(error); });
                    }
            }
        }, function (error) { return console.log(error); });
    };
    HomeComponent.prototype.closeHolidayModal = function (answer) {
        $('#holidayModal').fadeOut();
        if (answer === 'save' && this.modaltype === 'declare') {
            this.holidayStartDate = new Date(this.holidayStartDate + '');
            this.holidayEndDate = new Date(this.holidayEndDate + '');
            // var dates = [];
            // var month = this.holidayStartDate.getMonth();
            // var year = this.holidayStartDate.getFullYear();
            for (var index = this.holidayStartDate.getDate(); index <= this.holidayEndDate.getDate(); index++) {
                for (var j = 0; j < this.users.length; j++) {
                    this.setHoliday(new Date(this.holidayStartDate.getFullYear() + '-' + (this.holidayStartDate.getMonth() + 1) + '-' + index).getTime(), this.users[j]);
                }
            }
        }
        if (answer === 'save' && this.modaltype === 'cancel') {
            this.holidayStartDate = new Date(this.holidayStartDate + '');
            this.holidayEndDate = new Date(this.holidayEndDate + '');
            // var dates = [];
            // var month = this.holidayStartDate.getMonth();
            // var year = this.holidayStartDate.getFullYear();
            // for (var index = this.holidayStartDate.getDate(); index <= this.holidayEndDate.getDate(); index++) {
            // for (var j = 0; j < 1; j++) {
            // if (this.users.id === this.currentUser.id)
            // this.cancelHoliday(new Date(this.holidayStartDate.getFullYear() + '-' + (this.holidayStartDate.getMonth() + 1) + '-' + this.holidayStartDate.getDate()).getTime(), this.users[j])
            // }
            // }
        }
    };
    HomeComponent.prototype.displayHolidayModal = function (type) {
        this.modaltype = type;
        $('#holidayModal').fadeIn();
    };
    HomeComponent.prototype.clock = function () {
        //Save the times in variables
        var today = new Date();
        var hours = today.getHours();
        var minutes = today.getMinutes();
        var seconds = today.getSeconds();
        var minString;
        var secString;
        //Set the AM or PM time
        if (hours >= 12) {
            this.meridiem = " PM";
        }
        else {
            this.meridiem = " AM";
        }
        //convert hours to 12 hour format and put 0 in front
        if (hours > 12) {
            hours = hours - 12;
        }
        else if (hours === 0) {
            hours = 12;
        }
        //Put 0 in front of single digit minutes and seconds
        if (minutes < 10) {
            minString = "0" + minutes;
        }
        else {
            minString = minutes;
        }
        if (seconds < 10) {
            secString = "0" + seconds;
        }
        else {
            secString = seconds;
        }
        document.getElementById("clock").innerHTML = (hours + ":" + minString + ":" + secString + this.meridiem);
    };
    // events
    HomeComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    HomeComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    HomeComponent.prototype.makeMonthlyChart = function () {
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: 'home-cmp',
            moduleId: module.id,
            templateUrl: 'home.component.html',
            styleUrls: ['home.component.css']
        }),
        __metadata("design:paramtypes", [punch_service_1.PunchService])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=home.component.js.map