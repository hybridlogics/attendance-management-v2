import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { Routes } from '@angular/router';
import { LocalStorageModule } from 'angular-2-local-storage';


import { AppComponent }   from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './login/register.component';
import { UserComponent } from "./dashboard/user/user.component";
import { TableComponent } from "./dashboard/table/table.component";


import { DashboardModule } from './dashboard/dashboard.module';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';

import { HttpDataService } from './login/http-data.service';

const appRoutes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent },
];

@NgModule({
    imports:      [
        BrowserModule,
        DashboardModule,
        SidebarModule,
        NavbarModule,
        FooterModule,
        HttpModule,
        FormsModule,
        LocalStorageModule.withConfig({
            prefix: 'my-app',
            storageType: 'localStorage'
        }),
        RouterModule.forRoot( appRoutes )
    ],
    declarations: [ AppComponent, DashboardComponent, LoginComponent, RegisterComponent ],
    providers: [HttpDataService, LoginComponent, UserComponent, TableComponent],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }
