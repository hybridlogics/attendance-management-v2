import { Component, OnInit } from '@angular/core';
import { ROUTESAdmin, ROUTESEmployee } from './sidebar-routes.config';

declare var $:any;
@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    ngOnInit() {
        $.getScript('../../assets/js/sidebar-moving-tab.js');

        if(localStorage.getItem('userRole') === 'employee'){
            this.menuItems = ROUTESEmployee.filter(menuItem => menuItem);
        } else if(localStorage.getItem('userRole') === 'admin') {
            this.menuItems = ROUTESAdmin.filter(menuItem => menuItem);
        }
    }
}
