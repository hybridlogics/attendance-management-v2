"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ROUTESAdmin = [
    { path: 'dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
    { path: 'user', title: 'Users', icon: 'person', class: '' },
    { path: 'table', title: 'Analytics', icon: 'insert_chart', class: '' },
];
exports.ROUTESEmployee = [
    { path: 'dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
];
//# sourceMappingURL=sidebar-routes.config.js.map