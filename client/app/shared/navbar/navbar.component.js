"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var sidebar_routes_config_1 = require("../.././sidebar/sidebar-routes.config");
var common_1 = require("@angular/common");
var user_component_1 = require("../../dashboard/user/user.component");
var table_component_1 = require("../../dashboard/table/table.component");
var router_1 = require("@angular/router");
var punch_service_1 = require("../../dashboard/punch.service");
var NavbarComponent = (function () {
    function NavbarComponent(location, user, table, navigator, punch) {
        this.user = user;
        this.table = table;
        this.navigator = navigator;
        this.punch = punch;
        this.notifications = [];
        this.location = location;
    }
    NavbarComponent.prototype.ngOnInit = function () {
        if (localStorage.getItem('userRole') === 'employee') {
            this.listTitles = sidebar_routes_config_1.ROUTESEmployee.filter(function (listTitle) { return listTitle; });
        }
        else if (localStorage.getItem('userRole') === 'admin') {
            this.listTitles = sidebar_routes_config_1.ROUTESAdmin.filter(function (listTitle) { return listTitle; });
        }
        this.currentUser = JSON.parse(localStorage.getItem('my-app.id'));
        this.getAttendence();
    };
    NavbarComponent.prototype.getTitle = function () {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if (titlee.charAt(0) === '#') {
            titlee = titlee.slice(2);
        }
        for (var item = 0; item < this.listTitles.length; item++) {
            if (this.listTitles[item].path === titlee) {
                return this.listTitles[item].title;
            }
        }
        if (this.location.path() === '/user' && this.user.getShowFormStatus()) {
            return 'Back to Users';
        }
        if (this.location.path() === '/table' && this.table.getAnalyticsStatus()) {
            return 'Back to Users';
        }
        return 'Dashboard';
    };
    NavbarComponent.prototype.getAttendence = function () {
        var _this = this;
        var i = 0;
        this.punch.getEmployeeAttendence(this.currentUser).subscribe(function (data) {
            for (var index = 0; index < data.length; index++) {
                if (data[index].userId === _this.currentUser.userId && i < 5 && !data[index].reason) {
                    data[index].date = new Date(Number.parseInt(data[index].date));
                    data[index].punchIn = new Date((data[index].punchIn).toString());
                    data[index].punchOut = new Date((data[index].punchOut).toString());
                    _this.notifications.push(data[index]);
                    i++;
                }
            }
        }, function (error) { return console.log(error); });
    };
    NavbarComponent.prototype.logout = function () {
        localStorage.clear();
        this.navigator.navigateByUrl('/login');
    };
    NavbarComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            selector: 'navbar-cmp',
            templateUrl: 'navbar.component.html'
        }),
        __metadata("design:paramtypes", [common_1.Location, user_component_1.UserComponent, table_component_1.TableComponent, router_1.Router, punch_service_1.PunchService])
    ], NavbarComponent);
    return NavbarComponent;
}());
exports.NavbarComponent = NavbarComponent;
//# sourceMappingURL=navbar.component.js.map