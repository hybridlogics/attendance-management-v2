import { Component, OnInit } from '@angular/core';
import { ROUTESAdmin, ROUTESEmployee } from '../.././sidebar/sidebar-routes.config';
import {Location, LocationStrategy, PathLocationStrategy} from '@angular/common';
import { UserComponent } from "../../dashboard/user/user.component";
import { TableComponent } from "../../dashboard/table/table.component";
import { Router } from "@angular/router";
import { PunchService } from '../../dashboard/punch.service';
import { DatePipe } from '@angular/common';

@Component({
    moduleId: module.id,
    selector: 'navbar-cmp',
    templateUrl: 'navbar.component.html'
})

export class NavbarComponent implements OnInit{
    private listTitles: any[];
    location: Location;
    currentUser: any;
    notifications = [];
    constructor(location:Location, private user: UserComponent, private table: TableComponent, private navigator: Router, private punch: PunchService) {
        this.location = location;
    }
    ngOnInit(){
        if(localStorage.getItem('userRole') === 'employee'){
            this.listTitles = ROUTESEmployee.filter(listTitle => listTitle);
        } else if(localStorage.getItem('userRole') === 'admin') {
            this.listTitles = ROUTESAdmin.filter(listTitle => listTitle);
        }

        this.currentUser = JSON.parse(localStorage.getItem('my-app.id'))

        this.getAttendence();
    }
    getTitle(){
        var titlee = this.location.prepareExternalUrl(this.location.path());
        
        if(titlee.charAt(0) === '#'){
            titlee = titlee.slice( 2 );
        }
        for(var item = 0; item < this.listTitles.length; item++){
            if(this.listTitles[item].path === titlee){
                return this.listTitles[item].title;
            }
        }
        if(this.location.path() === '/user' && this.user.getShowFormStatus()) {
            return 'Back to Users';
        }
        if(this.location.path() === '/table' && this.table.getAnalyticsStatus()) {
            return 'Back to Users';
        }
        return 'Dashboard';
        
    }

    getAttendence() {
        var i = 0;
        this.punch.getEmployeeAttendence(this.currentUser).subscribe(
            data => {
                for (var index = 0; index < data.length; index++) {
                    
                    if(data[index].userId === this.currentUser.userId && i<5 && !data[index].reason) {
                        data[index].date = new Date(Number.parseInt(data[index].date))
                        data[index].punchIn = new Date((data[index].punchIn).toString())
                        data[index].punchOut = new Date((data[index].punchOut).toString())
                        this.notifications.push(data[index]);
                        i++;
                    }
                }
                },
            error => console.log(error)
        )

        
    }
    logout() {
        localStorage.clear();
        this.navigator.navigateByUrl('/login');
    }
}
