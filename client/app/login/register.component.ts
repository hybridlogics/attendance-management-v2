import {Component} from '@angular/core';
import {Router} from '@angular/router';
import { HttpDataService } from './http-data.service';

declare var $:any;

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html'
})

export class RegisterComponent {

    name: string;
    username: string;
    password: string;
    email: string;
    id: string;
    confirm_password: string;
    register: any;

    navigator: Router;
    constructor(_router: Router, public _httpService: HttpDataService) {
        this.navigator=_router;
    }

    login() {
        this.navigator.navigateByUrl('/login');
    }

    onRegister() {
    

        if(this.password!=this.confirm_password) {
            alert('Password doesnot match');
        }
        else {
            this.register = { "fullName":this.name, "username":this.username, "email":this.email, "password":this.password }

                this._httpService.registerEmployee(this.register)
                .subscribe(
                    data => {this.navigator.navigateByUrl('/login'); this.showNotificationRegistrationSuccess('top','right');},
                    error => this.showNotificationRegistrationFailed('top','right'),
                    () => console.log('done') 
                );
        }

    }

    showNotificationRegistrationFailed(from, align) {
        $.notify({
          icon: "notifications",
          message: "Registration Failed"
    
        },{
            type: 'warning',
            timer: 1000,
            delay: 2000,
            placement: {
                from: from,
                align: align
            }
        });
      }

      showNotificationRegistrationSuccess(from, align) {
        $.notify({
          icon: "notifications",
          message: "Registration Successfull"
    
        },{
            type: 'success',
            timer: 1000,
            delay: 2000,
            placement: {
                from: from,
                align: align
            }
        });
      }
}