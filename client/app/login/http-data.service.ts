import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';

@Injectable()

export class HttpDataService {

    constructor(private _http: Http) {}

    postJSON(login) {
        var json = JSON.stringify(login);
        var headers = new Headers({'Content-type': 'application/json'});
        
        return this._http.post('http://192.168.10.45:4567/api/AppUsers/login',json,{headers:headers})
        .map(res => res.json());
    }

    registerEmployee (register) {
        var json = JSON.stringify(register);
        var headers = new Headers({'Content-type': 'application/json'});
        
        return this._http.post('http://192.168.10.45:4567/api/AppUsers',json,{headers:headers})
        .map(res => res.json());
    }

    getEmployeeInfo(user) {
         return this._http.get('http://192.168.10.45:4567/api/AppUsers/' + user.userId + '?access_token=' + user.id)
        .map(res => res.json());
    }

    
}