"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var HttpDataService = (function () {
    function HttpDataService(_http) {
        this._http = _http;
    }
    HttpDataService.prototype.postJSON = function (login) {
        var json = JSON.stringify(login);
        var headers = new http_1.Headers({ 'Content-type': 'application/json' });
        return this._http.post('http://192.168.10.45:4567/api/AppUsers/login', json, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    HttpDataService.prototype.registerEmployee = function (register) {
        var json = JSON.stringify(register);
        var headers = new http_1.Headers({ 'Content-type': 'application/json' });
        return this._http.post('http://192.168.10.45:4567/api/AppUsers', json, { headers: headers })
            .map(function (res) { return res.json(); });
    };
    HttpDataService.prototype.getEmployeeInfo = function (user) {
        return this._http.get('http://192.168.10.45:4567/api/AppUsers/' + user.userId + '?access_token=' + user.id)
            .map(function (res) { return res.json(); });
    };
    HttpDataService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.Http])
    ], HttpDataService);
    return HttpDataService;
}());
exports.HttpDataService = HttpDataService;
//# sourceMappingURL=http-data.service.js.map