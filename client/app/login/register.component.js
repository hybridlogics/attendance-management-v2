"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var http_data_service_1 = require("./http-data.service");
var RegisterComponent = (function () {
    function RegisterComponent(_router, _httpService) {
        this._httpService = _httpService;
        this.navigator = _router;
    }
    RegisterComponent.prototype.login = function () {
        this.navigator.navigateByUrl('/login');
    };
    RegisterComponent.prototype.onRegister = function () {
        var _this = this;
        if (this.password != this.confirm_password) {
            alert('Password doesnot match');
        }
        else {
            this.register = { "fullName": this.name, "username": this.username, "email": this.email, "password": this.password };
            this._httpService.registerEmployee(this.register)
                .subscribe(function (data) { _this.navigator.navigateByUrl('/login'); _this.showNotificationRegistrationSuccess('top', 'right'); }, function (error) { return _this.showNotificationRegistrationFailed('top', 'right'); }, function () { return console.log('done'); });
        }
    };
    RegisterComponent.prototype.showNotificationRegistrationFailed = function (from, align) {
        $.notify({
            icon: "notifications",
            message: "Registration Failed"
        }, {
            type: 'warning',
            timer: 1000,
            delay: 2000,
            placement: {
                from: from,
                align: align
            }
        });
    };
    RegisterComponent.prototype.showNotificationRegistrationSuccess = function (from, align) {
        $.notify({
            icon: "notifications",
            message: "Registration Successfull"
        }, {
            type: 'success',
            timer: 1000,
            delay: 2000,
            placement: {
                from: from,
                align: align
            }
        });
    };
    RegisterComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: 'register.component.html'
        }),
        __metadata("design:paramtypes", [router_1.Router, http_data_service_1.HttpDataService])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map