import { Component, NgZone } from '@angular/core';
import { HttpDataService } from './http-data.service';
import { LocalStorageService } from 'angular-2-local-storage';
import {Router} from '@angular/router';
import { AppComponent }   from '../app.component';

declare var $:any;

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
   providers: [HttpDataService]
})

export class LoginComponent {
    postData: any;
  username: string;
  password: string;
  login: any;
  localStorage: LocalStorageService;
  navigator: Router;
err: boolean;
userInfo: any;

  constructor(_router: Router, localStorage: LocalStorageService, public _httpService: HttpDataService, public loggedStatus: AppComponent, private zone: NgZone) {
    loggedStatus.setLoggedStatus(false);
  
    
      this.err=false;
    this.localStorage = localStorage;
    this.navigator=_router;
    if(localStorage.get('id')) {
      loggedStatus.setLoggedStatus(true);
      this.navigator.navigateByUrl('/dashboard');
    }
  }

  showNotificationLogin(from, align) {
    $.notify({
      icon: "notifications",
      message: "Login Failed"

    },{
        type: 'warning',
        timer: 1000,
        delay: 2000,
        placement: {
            from: from,
            align: align
        }
    });
  }

  onPost() {

   this.login = { "email":this.username, "password":this.password }

    this._httpService.postJSON(this.login)
    .subscribe(data => {
      this.err = false;
      this.postData = data
      if(this.postData) {
        this.localStorage.set('id', this.postData);
          this._httpService.getEmployeeInfo(this.postData).subscribe(data => {
              this.userInfo = data;
              localStorage.setItem('userRole', this.userInfo.userRole);
              localStorage.setItem('fullName', this.userInfo.fullName);
            }, error => this.showNotificationLogin('top','right')
          );
        
        }     
      
    }, error => { this.showNotificationLogin('top','right'); this.err=true },
    () => { if(!this.err) {
              if(this.localStorage.get('id')) {
                this.loggedStatus.setLoggedStatus(true);
                this.zone.runOutsideAngular(() => {
                    location.reload();
                });
                this.navigator.navigateByUrl('/dashboard');
              }
            }
          }
    );

    

  }

  getLocalStorage() {
    return this.localStorage;
  }

  register() {
    this.navigator.navigateByUrl('/register');
  }
}