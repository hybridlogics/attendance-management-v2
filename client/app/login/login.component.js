"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_data_service_1 = require("./http-data.service");
var angular_2_local_storage_1 = require("angular-2-local-storage");
var router_1 = require("@angular/router");
var app_component_1 = require("../app.component");
var LoginComponent = (function () {
    function LoginComponent(_router, localStorage, _httpService, loggedStatus, zone) {
        this._httpService = _httpService;
        this.loggedStatus = loggedStatus;
        this.zone = zone;
        loggedStatus.setLoggedStatus(false);
        this.err = false;
        this.localStorage = localStorage;
        this.navigator = _router;
        if (localStorage.get('id')) {
            loggedStatus.setLoggedStatus(true);
            this.navigator.navigateByUrl('/dashboard');
        }
    }
    LoginComponent.prototype.showNotificationLogin = function (from, align) {
        $.notify({
            icon: "notifications",
            message: "Login Failed"
        }, {
            type: 'warning',
            timer: 1000,
            delay: 2000,
            placement: {
                from: from,
                align: align
            }
        });
    };
    LoginComponent.prototype.onPost = function () {
        var _this = this;
        this.login = { "email": this.username, "password": this.password };
        this._httpService.postJSON(this.login)
            .subscribe(function (data) {
            _this.err = false;
            _this.postData = data;
            if (_this.postData) {
                _this.localStorage.set('id', _this.postData);
                _this._httpService.getEmployeeInfo(_this.postData).subscribe(function (data) {
                    _this.userInfo = data;
                    localStorage.setItem('userRole', _this.userInfo.userRole);
                    localStorage.setItem('fullName', _this.userInfo.fullName);
                }, function (error) { return _this.showNotificationLogin('top', 'right'); });
            }
        }, function (error) { _this.showNotificationLogin('top', 'right'); _this.err = true; }, function () {
            if (!_this.err) {
                if (_this.localStorage.get('id')) {
                    _this.loggedStatus.setLoggedStatus(true);
                    _this.zone.runOutsideAngular(function () {
                        location.reload();
                    });
                    _this.navigator.navigateByUrl('/dashboard');
                }
            }
        });
    };
    LoginComponent.prototype.getLocalStorage = function () {
        return this.localStorage;
    };
    LoginComponent.prototype.register = function () {
        this.navigator.navigateByUrl('/register');
    };
    LoginComponent = __decorate([
        core_1.Component({
            moduleId: module.id,
            templateUrl: 'login.component.html',
            providers: [http_data_service_1.HttpDataService]
        }),
        __metadata("design:paramtypes", [router_1.Router, angular_2_local_storage_1.LocalStorageService, http_data_service_1.HttpDataService, app_component_1.AppComponent, core_1.NgZone])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map