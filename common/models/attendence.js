'use strict';

module.exports = function (Attendence) {

    //remote method to punchIn
    Attendence.punchIn = function (data, cb) {
    var today = new Date();
        Attendence.create({
            "userId": data.userId,
            "punchIn": new Date(),  
            "punchOut": null,
            "reason": "P",
            "date" : new Date(today.getFullYear(), today.getMonth() , today.getDate()).getTime(),
            "updatedAt": new Date()
        }, function(err, resp){

            if(err){
                console.log(err)
            }
            cb(err, resp);
        })
      
    }
    Attendence.remoteMethod('punchIn', {
        accepts: { arg: 'data', type: 'object' },
        returns: { arg: 'data', type: 'object' }
    });

    //remote method to punchOut
    Attendence.punchOut = function (data, cb) {
        var today = new Date();
        Attendence.updateAll({userId : data.userId, date: new Date(today.getFullYear(), today.getMonth() , today.getDate()).getTime() , punchOut : null, reason: "P"},{
            "punchOut": new Date(),
            "updatedAt": new Date(),
            "diff": new Date().getTime() - data.punchIn
        }, function(err, resp){

            cb(err, resp);
        })
      
    }
    Attendence.remoteMethod('punchOut', {
        accepts: { arg: 'data', type: 'object' },
        returns: { arg: 'data', type: 'object' }
    });

    // //remote method to punchIn
    Attendence.findPi = function ( cb) {
        
        Attendence.find({}, function(err, resp){

            
            for (var index = 0; index < resp.length; index++) {
                if (resp[index].punchOut && resp[index].punchIn) {
                    resp[index].timeDiff = (resp[index].punchOut).getTime() - (resp[index].punchIn).getTime();
                }
            }
            
            
            cb(err, resp);
        })
            
    }
    Attendence.remoteMethod('findPi', {
        returns: { arg: 'data', type: 'object' },
        http: {path: '/findPi', verb: 'get'}
    });
    
};
