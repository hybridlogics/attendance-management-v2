'use strict';
var path = require('path');

module.exports = function (server) {
    var router = server.loopback.Router();

    var states = [
        "/",
        "/login",
        "/register",
        "/dashboard",
		"/table",
        "/user"
    ]
    states.forEach(function (state) {
        router.get(state, function (req, res, next) {
            
            res.sendFile('index.html', {
                root: path.resolve(__dirname, '../..', 'client')
            });
        });
    })
    server.use(router);
};
